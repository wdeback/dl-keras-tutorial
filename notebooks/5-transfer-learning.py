
# coding: utf-8

# # Transfer learning
# 
# In practice, convolutional neural networks are often not trained from scratch (random initial conditions). Instead, an existing trained network is used and repurposed for a new image processing goal. 
# 
# In principle, there are two approaches to this:
# 
# 1. Transfer learning: re-use the feature extraction layers, fix the weights and biases, add a new classifier on top and only train this classifier on the new data. Recommended if you have only a small dataset.
# 2. Fine-tuning: re-train all weights of the network or, preferably, the top feature extraction layers. This requires that have sufficient new image data. Note that you will need a very small learning rate. 
# 
# Here, we will use the Inception v3 model that was trained on ImageNet dataset with 1000 categories. We will re-train it to distinguish cats and dogs (using a part of this data set: https://www.kaggle.com/c/dogs-vs-cats)
# 
# 
# 
# 

# # Set up 

# In[2]:

import matplotlib.pylab as plt
import numpy as np
import os, glob

from keras.applications import InceptionV3
from keras.preprocessing.image import ImageDataGenerator, img_to_array
from keras.layers import GlobalAveragePooling2D, Dense
from keras.models import Model, load_model, model_from_json


# In[31]:

train_dir = 'data/cats_vs_dogs/train'
val_dir = 'data/cats_vs_dogs/validate'
IM_WIDTH = 299
IM_HEIGHT = 299
FC_SIZE = 512 # number of nodes in Dense layer
batch_size = 16

output_filename = 'model/model.h5'
output_filename_json = 'model/model.json' 
output_filename_weights = 'model/weights.h5' 
coreml_modelname = 'model/inception_chairs3.mlmodel'


# ## Data augmentation

# In[32]:

# Inception expects image input scaled to [-1,1] interval
def preprocess_input(x):
    x /= 255.
    x -= 0.5
    x *= 2.
    return x

train_datagen =  ImageDataGenerator(
    preprocessing_function=preprocess_input,
    rotation_range=30,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True
)
test_datagen = ImageDataGenerator(
    preprocessing_function=preprocess_input,
    rotation_range=30,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True
)
train_generator = train_datagen.flow_from_directory(
  train_dir,
  target_size=(IM_WIDTH, IM_HEIGHT),
  batch_size=batch_size,
)
validation_generator = test_datagen.flow_from_directory(
  val_dir,
  target_size=(IM_WIDTH, IM_HEIGHT),
  batch_size=batch_size,
)

nb_classes = len(train_generator.class_indices)


# # Import Inception model and add new last layer

# In[33]:

# import Inception model without last layer (weights are downloaded if needed)
base_model = InceptionV3( weights='imagenet', include_top=False, input_shape=[299,299,3]) 

def add_new_last_layer(base_model, nb_classes):
    """Add last layer to the convnet
    Args:
    base_model: keras model excluding top
    nb_classes: # of classes
    Returns:
    new keras model with last layer
    """
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(FC_SIZE, activation='relu')(x) 
    predictions = Dense(nb_classes, activation='softmax')(x) 
    model = Model(inputs=base_model.input, outputs=predictions)
    return model

def setup_to_transfer_learn(model, base_model):
    """Freeze all layers and compile the model"""
    for layer in base_model.layers:
        layer.trainable = False
    model.compile(optimizer='adam',    
                loss='categorical_crossentropy', 
                metrics=['accuracy'])


# In[34]:

# add a Dense last layer + softmax activation
model = add_new_last_layer( base_model=base_model, nb_classes=nb_classes)

# compile model while freezing the base model layers
setup_to_transfer_learn(model=model, base_model=base_model)


# # Train the model

# In[37]:

nb_epoch = 10
nb_train_samples = 10
nb_val_samples = 2
verbose = 1

# continue training at last epoch, if any 
try:
    initial_epoch = len(history.history['acc'])
except:
    initial_epoch = 0

print("initial_epoch = ", initial_epoch)

if os.path.exists(output_filename):
    answer = raw_input('Do you want to load model from {}? [y/n] '.format(output_filename))
    if answer == 'y' or answer == 'yes':
        print('Loading model and weights from {}...'.format(output_filename))
        model = load_model(output_filename)
    else:
        print('Ignoring {}...'.format(output_filename))
        print('Warning: {} will be overwritten once training finishes'.format(output_filename))
        pass


# ## Train model

# In[39]:

history = model.fit_generator(
                    train_generator,
                    steps_per_epoch = nb_train_samples,
                    epochs = nb_epoch,
                    validation_data = validation_generator,
                    validation_steps = nb_val_samples,
                    verbose = verbose,
                    #use_multiprocessing = True,
                    initial_epoch = initial_epoch)


# ## Save retrained model 

# In[12]:

# save model architecure and weights 
model.save(output_filename)

# save model architecture as json file
import json
with open(output_filename_json, 'w') as outfile:
    model_json = model.to_json()
    json.dump(model_json, outfile)

# save model weights as hdf5 file
model.save_weights(output_filename_weights)


# ## Visualize training

# In[13]:

def plot_training(history):
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(len(acc))

    plt.figure()
    plt.plot(epochs, loss, 'r', label='loss')
    plt.plot(epochs, val_loss, 'b', label='validation loss')
    plt.title('Training and validation loss')
    plt.legend()

    plt.figure()
    plt.plot(epochs, acc, 'r', label='accuracy')
    plt.plot(epochs, val_acc, 'b', label='validation accuracy')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.show()

plot_training(history)


# # Prediction

# In[18]:

class_ind = train_generator.class_indices
CLASS_INDEX = dict((v,k) for k,v in class_ind.iteritems())

def decode_predictions(preds, top=3):
    results = []
    for pred in preds:
        top_indices = pred.argsort()[-top:][::-1]
        if pred[top_indices[0]] < 0.66:
            result = [tuple(["Not sure", pred[i]]) for i in top_indices]            
        elif (pred[top_indices[0]]-pred[top_indices[1]]) < 0.01:
            result = [tuple(["Ambiguous", pred[i]]) for i in top_indices]            
        else:
            result = [tuple([CLASS_INDEX[i], pred[i]]) for i in top_indices]
        
        results.append(result)
    return results
    
def predict(model, img, target_size, top_n=3):
    
    def preprocess_input2(x):
        x *= 2./255.
        x -= 1.
        return x

    """Run model prediction on image
    Args:
    model: keras model
    img: PIL format image
    target_size: (width, height) tuple
    top_n: # of top predictions to return
    Returns:
    list of predicted labels and their probabilities
    """
    if img.size != target_size:
        img = img.resize(target_size)

    x = img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input2(x)
    preds = model.predict(x)
    return decode_predictions(preds, top=top_n)[0]


# In[19]:

# pick random image sample
ratio = 0.5 # ratio of validation images
samples = [] 
datadir = 'data/validate'

directories = glob.glob( os.path.join(datadir, '*') )
directories_short = [d.split('/')[-1] for d in directories]
for d in directories_short:
    imfiles = glob.glob(os.path.join(datadir, d, '*.jpg'))
    total = len(imfiles)
    imfiles = np.array(imfiles)
    # determine number of files to move
    num = int(total * ratio)
    # get randomly chosen indices 
    idxs = np.random.choice(range(total), num, replace=False)
    # get filenames
    sourcefiles = imfiles[idxs].tolist()
    samples.extend(sourcefiles)

np.random.shuffle(samples)
print("Loaded {} sample images...".format(len(samples)))


# In[ ]:



