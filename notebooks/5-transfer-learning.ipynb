{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Transfer learning\n",
    "\n",
    "In practice, convolutional neural networks are often not trained from scratch (random initial conditions). Instead, an existing trained network is used and repurposed for a new image processing goal. \n",
    "\n",
    "In principle, there are two approaches to this:\n",
    "\n",
    "1. Transfer learning: re-use the feature extraction layers, fix the weights and biases, add a new classifier on top and only train this classifier on the new data. Recommended if you have only a small dataset.\n",
    "2. Fine-tuning: re-train all weights of the network or, preferably, the top feature extraction layers. This requires that have sufficient new image data. Note that you will need a very small learning rate. \n",
    "\n",
    "Here, we will use the Inception v3 model that was trained on ImageNet dataset with 1000 categories. We will re-train it to distinguish cats and dogs (using a part of this data set: https://www.kaggle.com/c/dogs-vs-cats)\n",
    "\n",
    "\n",
    "Code based on: https://deeplearningsandbox.com/how-to-use-transfer-learning-and-fine-tuning-in-keras-and-tensorflow-to-build-an-image-recognition-94b0b02444f2\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Set up "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pylab as plt\n",
    "import numpy as np\n",
    "import os, glob\n",
    "\n",
    "from keras.applications import InceptionV3\n",
    "from keras.preprocessing.image import ImageDataGenerator, img_to_array\n",
    "from keras.layers import GlobalAveragePooling2D, Dense\n",
    "from keras.models import Model, load_model, model_from_json"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "train_dir = '../data/cats_vs_dogs/train'\n",
    "val_dir = '../data/cats_vs_dogs/validate'\n",
    "IM_WIDTH = 299\n",
    "IM_HEIGHT = 299\n",
    "FC_SIZE = 512 # number of nodes in new Dense layer\n",
    "batch_size = 16\n",
    "\n",
    "output_filename = 'model/model.h5'\n",
    "output_filename_json = 'model/model.json' \n",
    "output_filename_weights = 'model/weights.h5' \n",
    "coreml_modelname = 'model/inception_chairs3.mlmodel'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data augmentation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Inception expects image input scaled to [-1,1] interval\n",
    "def preprocess_input(x):\n",
    "    x /= 255.\n",
    "    x -= 0.5\n",
    "    x *= 2.\n",
    "    return x\n",
    "\n",
    "train_datagen =  ImageDataGenerator(\n",
    "    preprocessing_function=preprocess_input,\n",
    "    rotation_range=30,\n",
    "    width_shift_range=0.2,\n",
    "    height_shift_range=0.2,\n",
    "    shear_range=0.2,\n",
    "    zoom_range=0.2,\n",
    "    horizontal_flip=True\n",
    ")\n",
    "test_datagen = ImageDataGenerator(\n",
    "    preprocessing_function=preprocess_input,\n",
    "    rotation_range=30,\n",
    "    width_shift_range=0.2,\n",
    "    height_shift_range=0.2,\n",
    "    shear_range=0.2,\n",
    "    zoom_range=0.2,\n",
    "    horizontal_flip=True\n",
    ")\n",
    "train_generator = train_datagen.flow_from_directory(\n",
    "  train_dir,\n",
    "  target_size=(IM_WIDTH, IM_HEIGHT),\n",
    "  batch_size=batch_size,\n",
    ")\n",
    "validation_generator = test_datagen.flow_from_directory(\n",
    "  val_dir,\n",
    "  target_size=(IM_WIDTH, IM_HEIGHT),\n",
    "  batch_size=batch_size,\n",
    ")\n",
    "\n",
    "nb_classes = len(train_generator.class_indices)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Import Inception model and add new last layer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# import Inception model without last layer (weights are downloaded if needed)\n",
    "base_model = InceptionV3( weights='imagenet', include_top=False, input_shape=[299,299,3]) \n",
    "\n",
    "def add_new_last_layer(base_model, nb_classes):\n",
    "    \"\"\"Add last layer to the convnet\n",
    "    Args:\n",
    "    base_model: keras model excluding top\n",
    "    nb_classes: # of classes\n",
    "    Returns:\n",
    "    new keras model with last layer\n",
    "    \"\"\"\n",
    "    x = base_model.output\n",
    "    x = GlobalAveragePooling2D()(x)\n",
    "    x = Dense(FC_SIZE, activation='relu')(x) \n",
    "    predictions = Dense(nb_classes, activation='softmax')(x) \n",
    "    model = Model(inputs=base_model.input, outputs=predictions)\n",
    "    return model\n",
    "\n",
    "def setup_to_transfer_learn(model, base_model):\n",
    "    \"\"\"Freeze all layers and compile the model\"\"\"\n",
    "    for layer in base_model.layers:\n",
    "        layer.trainable = False\n",
    "    model.compile(optimizer='adam',    \n",
    "                loss='categorical_crossentropy', \n",
    "                metrics=['accuracy'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add a Dense last layer + softmax activation\n",
    "model = add_new_last_layer( base_model=base_model, nb_classes=nb_classes)\n",
    "\n",
    "# compile model while freezing the base model layers\n",
    "setup_to_transfer_learn(model=model, base_model=base_model)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Train the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_epoch = 10\n",
    "nb_train_samples = 10\n",
    "nb_val_samples = 2\n",
    "verbose = 1\n",
    "\n",
    "# continue training at last epoch, if any \n",
    "try:\n",
    "    initial_epoch = len(history.history['acc'])\n",
    "except:\n",
    "    initial_epoch = 0\n",
    "\n",
    "print(\"initial_epoch = \", initial_epoch)\n",
    "\n",
    "if os.path.exists(output_filename):\n",
    "    answer = raw_input('Do you want to load model from {}? [y/n] '.format(output_filename))\n",
    "    if answer == 'y' or answer == 'yes':\n",
    "        print('Loading model and weights from {}...'.format(output_filename))\n",
    "        model = load_model(output_filename)\n",
    "    else:\n",
    "        print('Ignoring {}...'.format(output_filename))\n",
    "        print('Warning: {} will be overwritten once training finishes'.format(output_filename))\n",
    "        pass\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "history = model.fit_generator(\n",
    "                    train_generator,\n",
    "                    steps_per_epoch = nb_train_samples,\n",
    "                    epochs = nb_epoch,\n",
    "                    validation_data = validation_generator,\n",
    "                    validation_steps = nb_val_samples,\n",
    "                    verbose = verbose,\n",
    "                    #use_multiprocessing = True,\n",
    "                    initial_epoch = initial_epoch)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save retrained model "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save model architecure and weights \n",
    "model.save(output_filename)\n",
    "\n",
    "# save model architecture as json file\n",
    "import json\n",
    "with open(output_filename_json, 'w') as outfile:\n",
    "    model_json = model.to_json()\n",
    "    json.dump(model_json, outfile)\n",
    "\n",
    "# save model weights as hdf5 file\n",
    "model.save_weights(output_filename_weights)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_training(history):\n",
    "    acc = history.history['acc']\n",
    "    val_acc = history.history['val_acc']\n",
    "    loss = history.history['loss']\n",
    "    val_loss = history.history['val_loss']\n",
    "    epochs = range(len(acc))\n",
    "\n",
    "    plt.figure()\n",
    "    plt.plot(epochs, loss, 'r', label='loss')\n",
    "    plt.plot(epochs, val_loss, 'b', label='validation loss')\n",
    "    plt.title('Training and validation loss')\n",
    "    plt.legend()\n",
    "\n",
    "    plt.figure()\n",
    "    plt.plot(epochs, acc, 'r', label='accuracy')\n",
    "    plt.plot(epochs, val_acc, 'b', label='validation accuracy')\n",
    "    plt.title('Training and validation accuracy')\n",
    "    plt.legend()\n",
    "    plt.show()\n",
    "\n",
    "plot_training(history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Prediction\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "\n",
    "model = load_model('models/inception_cats_vs_dogs.h5')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class_ind = train_generator.class_indices\n",
    "CLASS_INDEX = dict((v,k) for k,v in class_ind.items())\n",
    "\n",
    "def decode_predictions(preds, top=3):\n",
    "    results = []\n",
    "    for pred in preds:\n",
    "        top_indices = pred.argsort()[-top:][::-1]\n",
    "        if pred[top_indices[0]] < 0.66:\n",
    "            result = [tuple([\"Not sure\", pred[i]]) for i in top_indices]            \n",
    "        elif (pred[top_indices[0]]-pred[top_indices[1]]) < 0.01:\n",
    "            result = [tuple([\"Ambiguous\", pred[i]]) for i in top_indices]            \n",
    "        else:\n",
    "            result = [tuple([CLASS_INDEX[i], pred[i]]) for i in top_indices]\n",
    "        \n",
    "        results.append(result)\n",
    "    return results\n",
    "    \n",
    "def predict(model, img, target_size, top_n=3):\n",
    "    \n",
    "    def preprocess_input2(x):\n",
    "        x *= 2./255.\n",
    "        x -= 1.\n",
    "        return x\n",
    "\n",
    "    \"\"\"Run model prediction on image\n",
    "    Args:\n",
    "    model: keras model\n",
    "    img: PIL format image\n",
    "    target_size: (width, height) tuple\n",
    "    top_n: # of top predictions to return\n",
    "    Returns:\n",
    "    list of predicted labels and their probabilities\n",
    "    \"\"\"\n",
    "    if img.size != target_size:\n",
    "        img = img.resize(target_size)\n",
    "\n",
    "    x = img_to_array(img)\n",
    "    x = np.expand_dims(x, axis=0)\n",
    "    x = preprocess_input2(x)\n",
    "    preds = model.predict(x)\n",
    "    return decode_predictions(preds, top=top_n)[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# pick random image sample\n",
    "ratio = 1.0 # ratio of validation images\n",
    "samples = [] \n",
    "datadir = '../data/cats_vs_dogs/validate'\n",
    "\n",
    "directories = glob.glob( os.path.join(datadir, '*') )\n",
    "print(directories)\n",
    "directories_short = [d.split('/')[-1] for d in directories]\n",
    "for d in directories_short:\n",
    "    imfiles = glob.glob(os.path.join(datadir, d, '*.jpg'))\n",
    "    total = len(imfiles)\n",
    "    imfiles = np.array(imfiles)\n",
    "    # determine number of files to move\n",
    "    num = int(total * ratio)\n",
    "    # get randomly chosen indices \n",
    "    idxs = np.random.choice(range(total), num, replace=False)\n",
    "    # get filenames\n",
    "    sourcefiles = imfiles[idxs].tolist()\n",
    "    samples.extend(sourcefiles)\n",
    "\n",
    "np.random.shuffle(samples)\n",
    "print(\"Loaded {} sample images...\".format(len(samples)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#def chunker(seq, size):\n",
    "#    return (seq[pos:pos + size] for pos in xrange(0, len(seq), size))\n",
    "\n",
    "for sample in samples:\n",
    "    from scipy.misc import imread\n",
    "    im2 = imread(sample)\n",
    "    print(sample, im2.shape)\n",
    "    from PIL import Image\n",
    "    im = Image.open(sample)\n",
    "    result = predict(model, im, (IM_WIDTH, IM_HEIGHT))\n",
    "    #print(result)\n",
    "    plt.imshow( im2 ) #np.rot90(im2,k=3) )\n",
    "    plt.axis('off')\n",
    "    plt.title('{} ({:.02f})'.format(result[0][0], result[0][1]))\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Evaluate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = model.evaluate_generator(validation_generator, steps=1)\n",
    "print('validation accuracy: {:.03f}'.format(results[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "174px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
